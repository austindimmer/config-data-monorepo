## Setup This Design Pattern in a New Repository

These instructions assume [GitLab Flow](https://docs.gitlab.com/ee/topics/gitlab_flow.html) for change managing environment promotion using GitOps methodologies.

### 1. Copy This Repository to Your Own Location

This can be done on a group on GitLab.com or on a self-hosted instance.

1. * [ ] Create a GitLab Group called "Project Top Scope" (this will be your "top scope" group)
1. * [ ] Create a subgroup under the original group called "Project Sub Scope" (this will be your subscope group)
1. * [ ] In the original repository, on the left navigation bar, click **Project overview => Details**
2. * [ ] From the upper right of the page click **Clone** (Dropdown button) 
3. * [ ] From the _Clone with HTTPS_ field, click the Copy Icon to copy the https:// repo url.
4. * [ ] While in the subgroup you created earlier, click the **"+"** icon at the top of the screen and select "New Project"
5. * [ ] From the "New Project" page, click **Import project** (tab)
6. * [ ] Click **Repo by URL**

> Note: if this option is missing it may be disabled in your instance, consult your GitLab instance administrator.

7. * [ ] In **Git repository URL** paste the copied URL. (Note: it must be https://)
8. * [ ] Set the following fields to your preferences: **Project name**, **Project URL**, **Project slug**, **Visibility Level**
9. * [ ] Click **Create project**
10. * [ ] In the new repository you made from this copy, restart this checklist as a GitLab issue so that: you can track your a) individual progress with b) an interactive checklist where you can checkoff each step, by:

### 2. Customize the Repository Settings

Please note that when "State = Protected" it is NOT write protecting the variable.  Instead, this is a switch the governs whether the specific variable is passed into "Protected Environments" or not. [Read more](https://docs.gitlab.com/ee/ci/variables/#protected-environment-variables).

In the "Project Top Scope"

1. * [ ] Update the group environment variables by clicking **Settings => CI/CD => Variables** and adding the below.
  
|   Type   | Key            | Value            | **Protected**<br />State | Masked | Scope |
| :------: | -------------- | ---------------- | :----------------------: | :----: | :---: |
| Variable | TOP_SCOPE_VAR1 | SET AT TOP SCOPE |           Off            |  Off   |  N/A  |
| Variable | TOP_SCOPE_VAR2 | SET AT TOP SCOPE |           Off            |   On   |  N/A  |
| Variable | TOP_SCOPE_VAR3 | SET AT TOP SCOPE |           Off            |   On   |  N/A  |

In the "Project Sub Scope"

1. * [ ] Update the group environment variables by clicking **Settings => CI/CD => Variables** and adding the below. 
  
|   Type   | Key            | Value                  | **Protected**<br />State | Masked | Scope |
| :------: | -------------- | ---------------------- | :----------------------: | :----: | :---: |
| Variable | SUBSCOPE_VAR1  | SET AT SUBGROUP SCOPE  |           Off            |  Off   |  N/A  |
| Variable | SUBSCOPE_VAR2  | SET AT SUBGROUP SCOPE  |           Off            |   On   |  N/A  |
| Variable | TOP_SCOPE_VAR1 | OVERRIDDEN AT SUBSCOPE |           Off            |   On   |  N/A  |

In the fresh repository copy:

1. * [ ] Update the project environment variables by clicking **Settings => CI/CD => Variables** and adding the below. 
  You must create the scopes by typing them in "search" and clicking "create" (which appears AFTER you type in a non-matching value). 

  > **Important:** The character case must exactly match your branch names (done as all lower throughout this example).

  |   Type   | Key                | Value                                          | **Protected**<br />State | Masked |      Scope       |
  | :------: | ------------------ | ---------------------------------------------- | :----------------------: | :----: | :--------------: |
  | Variable | GLOBAL_VISIBLE_VAR | VisibleData                                    |           Off            |  Off   | All environments |
  | Variable | MULTI_ENV_VAR      | Set In Envs That Start with S                  |           Off            |  Off   |        s*        |
  | Variable | PWD1               | ProdPswd                                       |           Off            |   On   |    production    |
  | Variable | PWD1               | SandboxPswd                                    |           Off            |   On   |     sandbox      |
  | Variable | PWD1               | StagePswd                                      |           Off            |   On   |      stage       |
  | Variable | REPO_SCOPE_VAR1    | Set at REPO Scope                              |           Off            |   On   | All environments |
  | Variable | SUBSCOPE_VAR2      | OVERRIDDEN AT REPO SCOPE                       |           Off            |   On   | All environments |
  | Variable | TOP_SCOPE_VAR2     | OVERRIDDEN AT SUBSCOPE AND AGAIN AT REPO SCOPE |           Off            |   On   | All environments |
  | Variable | TOP_SCOPE_VAR3     | OVERRIDDEN  AT REPO SCOPE                      |           Off            |   On   | All environments |
  | Variable | VISIBLE_ENV_DATA   | ProdVisible                                    |           Off            |  Off   |    production    |
  | Variable | VISIBLE_ENV_DATA   | SandboxVisible                                 |           Off            |  Off   |     sandbox      |
  | Variable | VISIBLE_ENV_DATA   | StageVisible                                   |           Off            |  Off   |      stage       |

1. * [ ] From the _left navigation_, click **Repository => Branches**

2. * [ ] There should be at least the branches listed in the table below. If any are missing, from the _upper right_, click **New branch** (button) and create any that are missing (must be lower case names).
  
   | Branch           | Allowed to merge         | Allowed to push          | Code owner approval |
   | ---------------- | ------------------------ | ------------------------ | ------------------- |
   | master [default] | Developers + Maintainers | Developers + Maintainers | Off                 |
   | production       | Maintainers *****        | No one                   | Off                 |
   | sandbox          | Developers + Maintainers | No one                   | Off                 |
   | Stage            | Developers + Maintainers | No one                   | Off                 |
   
   **\*** Can be set to a custom GitLab group in Premium and above.

3. * [ ] In **Settings => Repository => Protected Branches => Set Branch Protections according to the table in the previous step:**
  
3. * [ ] In **Settings => General => Merge Requests => UNCHECK: Enable 'Delete source branch' option by default**
    
  
5. * [ ] In **Settings => General => Merge Requests => CHECK: Pipelines must succeed**

6. * [ ] In **Settings => General => Merge Requests => CHECK: All discussions must be resolved**

7. * [ ] In **Settings => Repository => Default Branch => SELECT: 'Master'**

## 3 Create Personal Exploration Checklists
Guided explorations are implemented as GitLab issue templates so they can be used to create a personal progress tracking checklist for anyone wishing to walk through the exploration.

When in an issue, the checklist steps become clickable (without changing to markdown edit mode) and can be used to track personal progress by many individuals.

1. * [ ] On the left navigation of this repository click **Issues**
2. * [ ] Click **New issue**
3. * [ ] On the _New Issue_ page, next to _Title_, click **Choose a template** and select one of the templates containing the text **Guided-Exploration**
4. * [ ] The new issue created allows you to check items off as you complete them.